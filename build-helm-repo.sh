#!/usr/bin/env bash

set -e

helm package mayan-edms --app-version s4.0 --destination ./helmrepo
cd helmrepo
helm repo index .
